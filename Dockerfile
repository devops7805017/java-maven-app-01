FROM openjdk:8-jdk-alpine
RUN mkdir -p /home/app
COPY . /home/app
WORKDIR /home/app/target
CMD ["java", "-jar", "java-maven-app-1.1.0-SNAPSHOT.jar"]
